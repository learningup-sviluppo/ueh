module.exports = {
    LOGFORSCHOOL: `https://www.logforschool.it`,
    LOGFORSCHOOL_TESTING: `https://testing.logforschool.it`,
    DOCENDO: `https://www.docendoacademy.eu`,
    DOCENDO_TESTING: `https://testing-dot-docendo-2.oa.r.appspot.com`,//`https://testing.docendoacademy.eu`,
    SEMPLIFICACI: `https://app.semplificaci.com`,
    SEMPLIFICACI_TESTING: `https://testing.semplificaci.com`,
    PLUTONE: `https://plutone.app`,
    PLUTONE_TESTING: `https://testing.plutone.app`
};
