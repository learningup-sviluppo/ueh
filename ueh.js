`use strict`


const { v2beta3 } = require(`@google-cloud/tasks`);

const appUrls = require(`./appUrls.js`);

/*
    USAGE EXAMPLE:
        const myUehClient = new Ueh({
            projectId: `myproject-10`,
            locationId: `europe-west6`,
            credentials: {
                private_key: `ABCD1234`
                client_email: `clientaccount@myproject-10.iam.gserviceaccount.com`
            },
            serviceAccount: `myaccount@myproject-10.iam.gserviceaccount.com`
        });
*/
class Ueh {
    constructor({ projectId, locationId, credentials, serviceAccount }) {
        this.projectId = projectId;
        // ref.: https://cloud.google.com/about/locations/?tab=europe
        this.locationId = locationId;
        this.supportedMethods = [`GET`, `POST`];
        this.supportedAppURLs = appUrls;
        this.gCloudTasksClient = new v2beta3.CloudTasksClient({
            credentials: credentials
        });
        this.serviceAccount = serviceAccount;
    }

    // Return list of queues for the related project
    async getQueues() {
        try {
            // Retrieve the tasks list from the client
            const [queues] = await this.gCloudTasksClient.listQueues({
                parent: this.gCloudTasksClient.locationPath(this.projectId, this.locationId)
            });

            // Print the queues' names and details
            if (queues && queues.length > 0) {
                return queues;
            } else {
                return [];
            }
        } catch (error) {
            throw error;
        }
    }

    async getTasks({ queueName } = {}) {
        try {
            if (!queueName) throw new Error(`Must specify a valid queue name to list its tasks.`);

            const [tasks] = await this.gCloudTasksClient.listTasks({
                parent: this.gCloudTasksClient.queuePath(this.projectId, this.locationId, queueName)
            });

            // Print the queues' names and details
            if (tasks.length > 0) {
                return tasks;
            } else {
                return [];
            }
        } catch (error) {
            throw error;
        }
    }

    // const createTask = await docendoTasksClient.createTask("POST", "sendEmailTo_pippo@ciao.com", "* * * * *", "/sendEmailTo", "testqueue1", "testtaskX")
    // ref.: https://googleapis.dev/nodejs/tasks/latest/v2beta2.CloudTasksClient.html#createTask
    async createTask({ method, payload, schedule, endpoint, queue, name } = {}) {
        try {
            // If the method is not supported
            if (this.supportedMethods.indexOf(method) === -1)
                throw new Error(`Unsupported method.`);

            if (!schedule) throw new Error(`Trying to create a task without a schedule.`);

            if (!endpoint) throw new Error(`Trying to create a task without a dedicated endpoint.`);

            if (!queue) throw new Error(`Trying to create a task without specifying its queue.`);

            if (!payload) console.warn(`Creating a new task without a body.`);

            if (!name) name = `unnamedtask`;

            // Concatename a timestamp to our new task's name
            // to promote uniqueness amongst task names.
            name = `${name}_${Math.floor(Date.now() / 1000)}`;

            // Task name must be formatted like:
            //      "projects/<PROJECT_ID>/locations/<LOCATION_ID>/queues/<QUEUE_ID>/tasks/<TASK_ID>".
            name = `projects/${this.projectId}/locations/${this.locationId}/queues/${queue}/tasks/${name}`;

            // ref.: https://googleapis.dev/nodejs/tasks/latest/google.cloud.tasks.v2beta2.html#.Task
            const task = {
                name: name,
                appEngineHttpRequest: {
                    httpMethod: method,
                    relativeUri: endpoint,
                    body: Buffer.from(JSON.stringify({ payload: payload }))
                },
                // ref.: https://googleapis.dev/nodejs/tasks/latest/google.protobuf.html#.Timestamp
                scheduleTime: {
                    seconds: schedule
                }
            };

            // Use Google's client to send the job creation request.
            const [response] = await this.gCloudTasksClient.createTask({
                parent: this.gCloudTasksClient.queuePath(this.projectId, this.locationId, queue),
                task: task
            } );

            return response;
        } catch (error) {
            throw error;
        }
    }

    /*
        USAGE EXAMPLE:
            const newTask = await tasksClient.createHTTPTask({
                name: `testtaskname`
                method: `POST`,
                payload: `TEST_${Date.now().toString(16).substr(2)}`,
                schedule: require(`moment`)().format(`X`),
                appName: `Docendo_testing`,
                endpoint: `/tasks/courses/sendCoursePreparationEmail`,
                queue: `testqueue1`
            });
    */
    async createHTTPTask({ name, method, payload, schedule, appName, context, endpoint, queue } = {}) {
        try {
            if (this.supportedMethods.indexOf(method) === -1)
                throw new Error(`Please use a valid HTTP request method.`);

            if (!schedule) throw new Error(`Trying to create a task without a schedule.`);

            if (!endpoint) throw new Error(`Trying to create a task without a dedicated endpoint.`);

            if (!queue) throw new Error(`Trying to create a task without specifying its queue.`);

            if (!payload) console.warn(`Creating a new task without a body.`);

            if (!Object.prototype.hasOwnProperty.call(this.supportedAppURLs, appName.toUpperCase()))
                throw new Error(`Invalid app name.`);

            if (!name) name = `unnamedtask`;

            if (!context) console.warn(`Defaulting to "testing" context.`);

            // Concatenate a timestamp to our new task's name
            // to promote uniqueness amongst task names.
            name = `${name}_${Math.floor(Date.now() / 1000)}`;

            name = `projects/${this.projectId}/locations/${this.locationId}/queues/${queue}/tasks/${name}`;

            appName = (context && context == `production`) ? appName : `${appName}_testing`;

            // Fetch the base URL for the given app
            const appBaseURL = this.supportedAppURLs[appName.toUpperCase()];

            const task = {
                name: name,
                httpRequest: {
                    httpMethod: method,
                    url: appBaseURL + endpoint,
                    body: payload ? Buffer.from(JSON.stringify({ payload: payload })) : undefined,
                    headers: {
                        "Content-Type": "application/json"
                    },
                    oidcToken: {
                        serviceAccountEmail: this.serviceAccount
                    }
                },
                scheduleTime: {
                    seconds: schedule
                }
            };

            // Use Google's client to send the job creation request.
            const [response] = await this.gCloudTasksClient.createTask({
                parent: this.gCloudTasksClient.queuePath(this.projectId, this.locationId, queue),
                task: task
            });

            return response;
        } catch (error) {
            throw error;
        }
    }

    async createQueue(newQueueName, serviceName) {
        try {
            // Send create queue request.
            const [response] = await this.gCloudTasksClient.createQueue({
                // The fully qualified path to the location where the queue is created
                parent: this.gCloudTasksClient.locationPath(this.projectId, this.locationId),
                queue: {
                    // The fully qualified path to the queue
                    name: this.gCloudTasksClient.queuePath(
                        this.projectId,
                        this.locationId,
                        newQueueName
                    ),
                    appEngineHttpQueue: {
                        appEngineRoutingOverride: {
                            // The App Engine service that will receive the tasks.
                            service: serviceName
                        }
                    }
                }
            });

            return response;
        } catch (error) {
            throw error;
        }
    }
}

module.exports = { Ueh };
